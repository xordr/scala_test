package com.xordr

import java.io.{FileWriter, PrintWriter}
import scala.io.Source


object Main {

  def main(args: Array[String]): Unit = {

    val input = Source.fromFile(getClass.getResource("input").getPath)

    // bm start
    val start = System.currentTimeMillis()

    /**

                         1 - - 2  1 - - 2
       t1 ->             |  1  |  |  2  |
                         3 - - 4  3 - - 4
                1 - - 2  1 - - 2  1 - - 2  1 - - 2
       t2 ->    |  3  |  |  4  |  |  5  |  |  6  |
                3 - - 4  3 - - 4  3 - - 4  3 - - 4
                1 - - 2  1 - - 2  1 - - 2  1 - - 2
       t2 ->    |  7  |  |  8  |  |  9  |  | 1 0 |
                3 - - 4  3 - - 4  3 - - 4  3 - - 4
                         1 - - 2  1 - - 2
       t1 ->             | 1 1 |  | 1 2 |
                         3 - - 4  3 - - 4

      */


    val squares: List[Square] = input
      .getLines().toList
      .map(line => line.split(" ").map(str => str.toInt))
      .map(arr => new Square((arr(0), arr(1), arr(2), arr(3))))

    // all possible permutations of 12 squares for 2 cells
    val t1 = permute(squares, size = 2)
    // all possible permutations of 12 squares for 4 cells
    val t2 = permute(squares, size = 4)

    val filter1_2: List[Square] => Boolean = sq => (sq(0).element._2 + sq(1).element._1) <= 10
    val filter11_12: List[Square] => Boolean = sq => (sq(0).element._4 + sq(1).element._3) <= 10

    val redundant: List[Square] => Boolean = n => n.nonEmpty && n.toSet.size == n.length

    def f1_6(set1: List[Square], set2: List[Square]): Boolean = {
      (set1(0).element._4 + set1(1).element._3 + set2(1).element._2 + set2(2).element._1) == 10 &&
        (set1(0).element._3 + set2(0).element._2 + set2(1).element._1) <= 10 &&
        (set1(1).element._4 + set2(2).element._2 + set2(3).element._1) <= 10
    }

    def f1_10(set1: List[Square], set2: List[Square]): Boolean = {
      (set1(2).element._4 + set1(3).element._3 + set2(0).element._2 + set2(1).element._1) == 10 &&
        (set1(3).element._4 + set1(4).element._3 + set2(1).element._2 + set2(2).element._1) == 10 &&
        (set1(4).element._4 + set1(5).element._3 + set2(2).element._2 + set2(3).element._1) == 10 &&
        (set1(2).element._3 + set2(0).element._1) <= 10 &&
        (set1(5).element._4 + set2(3).element._2) <= 10
    }

    def f1_12(set1: List[Square], set2: List[Square]): Boolean = {
      (set1(6).element._4 + set1(7).element._3 + set2(0).element._1) <= 10 &&
        (set1(8).element._4 + set1(9).element._3 + set2(1).element._2) <= 10 &&
        (set1(7).element._4 + set1(8).element._3 + set2(0).element._2 + set2(1).element._1) == 10
    }

    // checked combinations of the first two cells
    val checked1_2 = t1.filter(filter1_2)

    // checked combinations of the last two cells
    val checked11_12 = t1.filter(filter11_12)

    // checked combinations of the first 6 cells
    val checked1_6 = checked1_2
      .flatMap(set1 => t2.filter(set2 => f1_6(set1, set2)).map(ch2 => set1 ++ ch2))
      .filter(redundant)

    // checked combinations of the first 10 cells
    val checked1_10 = checked1_6
      .flatMap(set1 => t2.filter(set2 => f1_10(set1, set2)).map(ch2 => set1 ++ ch2))
      .filter(redundant)

    // checked combinations of the first 12 cells (final)
    val check1_12 = checked1_10
      .flatMap(set1 => checked11_12.filter(set2 => f1_12(set1, set2)).map(ch2 => set1 ++ ch2))
      .filter(redundant)

    // bm end
    val time = (System.currentTimeMillis() - start).toDouble / 1000

    check1_12.foreach(set => println(set.mkString(" \n") + "\n"))

    println(f"Computation time: " + "%.3f".format(time) + " s")

  }

  def permute[T](l: List[T], size: Int): List[List[T]] = {
    (size to size).flatMap(i => l.combinations(i).flatMap(_.permutations)).toList
  }


  class Square(val element: (Int, Int, Int, Int)) {
    override def toString: String = element._1 + " " + element._2 + " " + element._3 + " " + element._4

    override def equals(obj: scala.Any): Boolean = {
      val that = obj.asInstanceOf[Square]
      this.hashCode == that.hashCode
    }
  }

}
